import React from 'react'
import { createSwitchNavigator, createStackNavigator } from 'react-navigation'
import DrawerStack from '../stacks/DrawerStack'
import AuthStack from '../stacks/AuthStack'
import LoadingStack from '../stacks/LoadingStack'
import GameStack from '../stacks/GameStack'
import Welcome from '../screens/start/StartScreen'
import profile from '../screens/edit-profile/EditProfileScreen'

export default createStackNavigator({
  LoadingStack: LoadingStack,
  AuthStack: AuthStack,
  DrawerStack: DrawerStack,
  GameStack: GameStack,
  Welcome: Welcome,

}, {
  initialRouteName: 'Welcome',
  navigationOptions: () => ({
    header: null
  })
})

