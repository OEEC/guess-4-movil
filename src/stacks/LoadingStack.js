import React from 'react'
import { createStackNavigator } from 'react-navigation'
import LoadingComponent from '../components/LoadingComponent'


let LoadingStack = createStackNavigator({
  Loading: LoadingComponent
},{
  navigationOptions: () => ({
    header: null
  })
})

export default LoadingStack
