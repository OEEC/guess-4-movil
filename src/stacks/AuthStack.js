import React from 'react'
import { createStackNavigator} from 'react-navigation'
import LoginScreen from '../screens/login/LoginScreen'
import RegisterScreen from '../screens/register/RegisterScreen'
import ConfirmCodeScreen from '../screens/confirm-code/ConfirmCodeScreen'
import StartScreen from '../screens/start/StartScreen'

let AuthStack = createStackNavigator({
  Start: StartScreen,
  Login: LoginScreen,
  Register: RegisterScreen,
  ConfirmCode: ConfirmCodeScreen
},{
  navigationOptions: () => ({
    header: null
  })
})

export default AuthStack
