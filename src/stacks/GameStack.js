import React from 'react'
import { createStackNavigator } from 'react-navigation'
import StartScreen from '../screens/start/StartScreen'
import GameScreeen from '../screens/game/GameScreen'
import AdScreen from '../screens/ad/AdScreen'
import CongratulationScreen from '../screens/congratulation/CongratulationScreen'

let GameStack = createStackNavigator({
  Game: GameScreeen,
  Ad: AdScreen,
  Congratulation: CongratulationScreen
}, {
  navigationOptions: () => ({
    header: null
  })
})

export default GameStack
