import React from 'react'
import { SafeAreaView, DrawerItems, createStackNavigator, createDrawerNavigator } from 'react-navigation'
import { View, Text, Image, ScrollView, TouchableOpacity, AsyncStorage, Platform } from 'react-native'
import { Icon,Thumbnail }   from 'native-base'
import HomeScreen from '../screens/home/HomeScreen'
import HistoryScreen from '../screens/history/HistoryScreen';
import InstructionsScreen from '../screens/instructions/InstructionsScreen';
import ProfileScreen from '../screens/profile/ProfileScreen';
import RankingScreen from '../screens/ranking/RankingScreen';
import EditProfileScreen from '../screens/edit-profile/EditProfileScreen'
import PromcodeScreen from '../screens/promcode/PromcodeScreen'
import LoginScreen from '../screens/login/LoginScreen';

var authUser = null
var photoURL = null

let getData = async () => {
  let user  = await AsyncStorage.getItem('user')
  let photo = await AsyncStorage.getItem('photo')
  authUser  = user
  photoURL  = photo
}

let CustomDrawer = (props) => {
  getData()
  return(
    <SafeAreaView style={{flex: 1}}>
      <View style={{height: 150,flexDirection:"row",alignItems: 'center', marginHorizontal: 20, justifyContent: 'center', borderBottomWidth: 1}}>
        <View style={{flexDirection:"column",alignItems:"center",justifyContent:"center",flex:1,padding:5,overflow: 'hidden'}}>
            <Thumbnail source={{ uri:  `http://192.168.137.1:8000/assets/images/users/${photoURL}`}} />
          <Text 
          onPress={() => {
            props.navigation.navigate('Profile')
          }}
          style={{ fontSize: 16, color: '#532b64', fontWeight: 'bold', marginTop: 1}}>{authUser}</Text>
        </View>
      </View>
      <ScrollView>
        <DrawerItems {...props} />
        <TouchableOpacity onPress={() => signOut(props)} style={{padding: 15, flex: 1, flexDirection: 'row'}}>
          <Icon name="md-log-out" style={{color: '#ad59be'}}/>
          <View style={{marginLeft: '1.5%', justifyContent: 'center'}}>
            <Text style={{fontWeight: 'bold', color: '#ad59be'}}>Cerrar sesión</Text>
          </View>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  )
  }

let DrawerScreen = createDrawerNavigator({
  Home: {
    screen: HomeScreen
  },
  History: {
    screen: HistoryScreen
  },
  Instructions: {
    screen: InstructionsScreen
  },
  Profile: {
    screen: ProfileScreen
  },
  Ranking: {
    screen: RankingScreen
  },
  EditProfile: {
    screen: EditProfileScreen
  },
  Code:{
    screen: PromcodeScreen
  }
}, {
  initialRouteName: 'Home',
  contentOptions: {
    inactiveTintColor: '#b2b2b2',
    activeTintColor: '#532b64'
  },
  contentComponent: CustomDrawer,
  style: {
    marginTop: 15
  },
  navigationOptions: () => ({
    header: null
  })
})

export default createStackNavigator({
  DrawerStack: {
    screen: DrawerScreen
  }
}, {
  headerMode: 'none',
  navigationOptions: ({navigation}) => ({
    gesturesEnabled: true
  })
})

let signOut = (props) => {
  // firebase.auth().signOut()
  let key   = ['id', 'token', 'user']
  
  AsyncStorage.multiRemove(key).then(res =>{
    props.navigation.navigate('Start')
  })
}