import React, { Component } from 'react'
import { TouchableOpacity } from 'react-native'
import {Alert, View, Modal, Text, TouchableHighlight, ImageBackground, 
        Image, Animated, Easing, AsyncStorage, TextInput, withNavigation} from 'react-native'
import { Container, Content, Icon, Toast, Button } from 'native-base'
import HeaderComponent from '../../components/HeaderComponent'
import style from './PromcodeStyleSheet'
import axios from 'react-native-axios'
import moment from 'moment/min/moment-with-locales'
import {URL} from '../../config/env'





export default class PromcodeScreen extends Component {

  constructor(props) {
    super(props)
    this.state = {
       ctext: "",
       mtext: "",
       m2text: "",
       code:    "",
       codeid: "",
       pcode: "",
       codeidp: "",
       goTo: "",
       modalVisible: false,
    }
    this.animatedValue = new Animated.Value(0)
    
  }
  
  componentDidMount(){

    this.animate()
   }

  animate() {
    this.animatedValue.setValue(0)
    Animated.timing(
      this.animatedValue, 
      {
        toValue: 1,
        duration: 900,
        easing: Easing.linear
      }
    ).start()
  }
  

  getCode = async () => {
    let that = this
    that.state.pcode   = await AsyncStorage.getItem('pcode')
    
    const user_id   = await AsyncStorage.getItem('id')
     console.log(that.state.code)
     console.log(user_id)
     console.log(that.state.pcode)
    if(that.state.code!='')
     { 
       //verifica si va a utilizar codigo personal propio
         if(that.state.code != that.state.pcode.toString()){ 

           let code = {'code': that.state.code, 'idu': user_id}
            axios({
              method:'POST',
              url: `${URL}/post-comparedcodes`,
              headers: {
                'Content-Type': 'application/json'
              },
              data: code
            })
            .then(async function (response) {
                console.log('regreso')
                console.log(response.data.message)
               
                that.state.codeid=response.data.idprom.toString();
                await AsyncStorage.setItem('idprom', that.state.codeid);
                var algo = await AsyncStorage.getItem('idprom');
               
                console.log(algo)
                console.log(response.data.message)
                  if(response.data.response==false){
                        that.state.ctext="¡UPS!"
                        that.state.mtext=response.data.message
                        that.state.m2text=''
                        that.setModalVisible(true);
                        that.state.goTo='Code'
                  }else if(response.data.response==true){
                    that.state.ctext="¡Genial!"
                    that.state.mtext=response.data.message
                    that.state.m2text=''
                    that.setModalVisible(true);
                    that.state.goTo='Home'
                  }

                  
              //   if( response.data.message==='Codigo Invalido'){
              //     console.log('sin codigo')
                
              //     that.state.ctext="¡UPS!"
              //     that.state.mtext=response.data.message
              //     that.state.m2text=''
              //     that.setModalVisible(true);
              //     that.state.goTo='Code'
              //   }else if(response.data.message==='Codigo Expirado'){
              //     that.state.ctext="¡UPS!"
              //     that.state.mtext=response.data.message
              //     that.state.m2text=''
              //     that.setModalVisible(true);
              //     that.state.goTo='Code'
              //   }else if(response.data.message==='Tu Codigo se a Canjeado'){
              //     that.state.ctext="¡Genial!"
              //     that.state.mtext=response.data.message
              //     that.state.m2text=that.state.codeid
              //     that.setModalVisible(true);
              //     that.state.goTo='Home'
              //   }else if(response.data.message==='Tu codigo se a revalidado'){
              //     that.state.ctext="¡Genial!"
              //     that.state.mtext=response.data.message
              //     that.state.m2text=''
              //     that.setModalVisible(true);
              //     that.state.goTo='Home'
              //   }else if(response.data.message==='Codigo ya se a Utilizado'){
              //     that.state.ctext="¡UPS!"
              //     that.state.mtext=response.data.message
              //     that.state.m2text=''
              //     that.setModalVisible(true);
              //     that.state.goTo='Code'
              //   }else if(response.data.message==='Problemas al Validar Codigo'){
              //     that.state.ctext="¡UPS!"
              //     that.state.mtext=response.data.message
              //     that.state.m2text=''
              //     that.setModalVisible(true);
              //     that.state.goTo='Code'
              //   }else{
              // console.log('sin codigo')
              // that.state.ctext="¡UPS!"
              // that.state.mtext=response.data.message
              // that.state.m2text=''
              // that.setModalVisible(true);
              // that.state.goTo='Home'
              //   }           
            })
            .catch(function (error) {
              console.log('sin codigo')
              that.state.ctext="¡UPS!"
              that.state.mtext=error.message
              that.state.m2text=''
              that.setModalVisible(true);
              that.state.goTo='Home'
            
            });
            }else{
      
                  that.state.ctext="¡Error de Codigo!"
                  that.state.mtext="Estas tratando de utilizar tu"
                  that.state.m2text="codigo personal en ti mismo"
                  that.setModalVisible(true);
                  that.state.goTo='Code'
            }
          }else{
        
            that.state.ctext="¡UPS!"
            that.state.mtext="Por favor introduce un codigo"
            that.state.m2text=''
            that.setModalVisible(true);
            that.state.goTo='Code'
          }
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }


  render() {
    const {navigate} = this.props.navigation
    let opacity = this.animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 1, 1]
    })
    let marginTop = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [-40, 0]
    })
    let scale = this.animatedValue.interpolate({
      inputRange: [0, 0.3],
      outputRange: [0.4, 0.58]
    })
    let {navigation} = this.props;
    return (
        <Container>
          <ImageBackground source={require('../../assets/images/fondo-min.png')} style={{width: '100%', height: '100%'}}>
            <HeaderComponent navigation={navigation}/>
            <Content>
              <View style={style.container}>
                <Animated.View style={[style.boxContainerLogo, { opacity }]}>
                  <Image source={require('../../assets/images/guess4-logo-1024-min.png')} style={style.logo}/>
                </Animated.View>

                <View style={style.cardMessage} >
                    <Text style={[style.textMessageBold, {fontFamily: 'Roboto'}]}>¡Introduce tu codigo!</Text>
										<TextInput
												placeholder="codigo"
												returnKeyType="next"
												autoCapitalize={'none'}
												onChangeText={ (code) => this.setState({code})}
												style={style.input}
												keyboardType="email-address"/>
                </View>
						
								<Button rounded small block iconRight style={{ backgroundColor: '#a903bd' }} onPress={this.getCode}>
								  <Icon name="gamepad" type="FontAwesome" style={style.iconShare}/>
									<Text style={{ color: '#FFF', textAlignVertical: 'center', marginBottom: '1%', marginTop: 3 }}>Introducir </Text>
									<Icon name="chevron-right" type='FontAwesome' style={{ color: '#FFF', fontSize: 16, marginTop: 3 }} />
								</Button>

                {/* <TouchableHighlight
                  onPress={() => {
                    this.setModalVisible(true);
                  }}>
                  <Text>Show Modal</Text>
                </TouchableHighlight> */}
                {/* modal */}
               <View style={style.containerModal}>
                <Modal
                  animationType="slide"
                  transparent={true}
                  visible={this.state.modalVisible}
                 >
                  <View style={style.modalContainer}>
                    <View style={style.innerContainer}>
                    <View>
                    <Text style={[style.textTitle, {fontFamily: 'Roboto'}]}>{this.state.ctext}</Text>
                    </View>
                      <Text style={[style.textMessage, {fontFamily: 'Roboto'}]}>{this.state.mtext}</Text>
                      <Text style={[style.textMessage2, {fontFamily: 'Roboto'}]}>{this.state.m2text}</Text>
                      <Button rounded small block iconRight style={{ backgroundColor: '#a903bd', marginTop: 3 }} onPress={() => {
                          this.setModalVisible(!this.state.modalVisible); navigate(this.state.goTo);
                        }}>
								    		<Text style={{ color: '#FFF', textAlignVertical: 'center', marginBottom: '1%', marginTop: 3}}
                         >Aceptar</Text>
								    	</Button>
                  
                    </View>
                  </View>
                </Modal>
                </View>
              </View>
            </Content>
          </ImageBackground>
        </Container>
    )
  }


}
