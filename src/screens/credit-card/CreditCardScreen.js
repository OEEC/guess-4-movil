import React, { Component } from 'react'
import { TextInput, TouchableOpacity, View, ImageBackground } from 'react-native'
import { Container, Content, Text } from 'native-base'
import style from './CreditCardStyleSheet'

export default class CreditCardScreen extends Component {
  render() {
    let { navigate } = this.props.navigation;
    return (
        <ImageBackground source={require('../../assets/images/fondo-min.png')} style={{width: '100%', height: '100%'}}>
          <View style={style.container} >
            <View style={{flex: 1, width: '95%', alignContent: 'space-between', alignItems: 'center'}}>
              <Text style={style.textCongrats}>Felicidades</Text>
              <View style={style.boxText}>
                <Text style={style.text}>Tú patron ha sido</Text>
                <Text style={style.text2}>el ganador.</Text>
              </View>
              <TextInput
                style={style.input}
                maxLength={16}
                keyboardType="numeric"
                textContentType="creditCardNumber"
                placeholder="Número de tarjeta de credito"/>
              <TouchableOpacity style={style.button} onPress={() => navigate('DrawerStack')}>
                <Text style={style.textButton}>Enviar</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
    )
  }
}
