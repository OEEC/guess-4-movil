import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
    flexDirection: 'column',
    padding: 25
  },
  textCongrats: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#FAFAFA'
  },
  boxText: {
    margin: '15%',
    alignItems: 'center'
  },
  text: {
    color: '#FAFAFA',
    fontSize: 18
  },
  text2: {
    marginTop: -6,
    color: '#FAFAFA',
    fontSize: 18
  },
  input: {
    backgroundColor: '#FAFAFA',
    borderRadius: 4,
    color: '#532b64',
    fontWeight: 'bold',
    width: '100%',
    textAlign: 'center'
  },
  button: {
    margin: '12%',
    borderRadius: 50,
    backgroundColor: '#ad59be',
    padding: 5
  },
  textButton: {
    marginStart: '15%',
    marginEnd: '15%',
    fontSize: 15,
    color: '#ffffff',
    fontWeight: 'bold',
    textAlign: 'center'
  }
})