import React, { Component } from 'react'
import { ImageBackground, View, TouchableOpacity, Text, TextInput, Image } from 'react-native'
import { Toast }  from 'native-base'
import { URL }    from '../../config/env'
import Style      from './RegisterStyleSheet'
import axios      from 'axios'
import Icon       from 'react-native-vector-icons/FontAwesome'
import Spinner    from 'react-native-loading-spinner-overlay'

export default class RegisterScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      username: '',
      password: '',
      retypePassword: '',
      phoneNumber: '',
      loading: false
    }
  }

  render() {
    let {navigate} = this.props.navigation;
    return (
      <ImageBackground source={require('../../assets/images/fondo-min.png')} style={{width: '100%', height: '100%'}} >
        <Spinner
          visible={this.state.loading}
          textContent={'Espera!!!...'}
          textStyle={Style.spinnerTextStyle}
        />
        <View style={Style.container}>
          <View style={Style.card}>
            <Image source={require('../../assets/images/guess4-logo-1024-min.png')} style={Style.logo} ></Image>
            <View style={Style.SectionStyle}>
              <Icon style={Style.icon} name="envelope" size={20}/>
              <TextInput
                placeholder="Correo electrónico"
                returnKeyType="next"
                autoCapitalize={'none'}
                keyboardType="email-address"
                onSubmitEditing={ () => { this.usernameInput.focus() }}
                onChangeText={ (email) => this.setState({email})}
                style={Style.input}
                />
            </View>
            
            <View style={Style.SectionStyle}>
              <Icon style={Style.icon} name="user" size={20}/>
              <TextInput
                placeholder="Nombre de usuario"
                returnKeyType="next"
                style={Style.input}
                onSubmitEditing={ () => { this.passwordInput.focus() }}
                onChangeText={(username) => this.setState({username}) }
                ref={(input) => { this.usernameInput = input }}
                /> 
            </View>

            <View style={Style.SectionStyle}>
              <Icon style={Style.icon} name="key" size={20}/>
              <TextInput
                placeholder="Contraseña"
                returnKeyType="next"
                style={Style.input}
                secureTextEntry
                onSubmitEditing={() => { this.retypePasswordInput.focus() }}
                onChangeText={ (password) => this.setState({password}) }
                ref={(input) => { this.passwordInput = input }}
                />
            </View>
            
            <View style={Style.SectionStyle}>
              <Icon style={Style.icon} name="key" size={20}/>
              <TextInput
                placeholder="Repite la contraseña"
                returnKeyType="next"
                style={Style.input}
                secureTextEntry
                onSubmitEditing={() => { this.phoneNumberInput.focus() }}
                onChangeText={(retypePassword) => this.setState({retypePassword})}
                ref={(input) => { this.retypePasswordInput = input }}
                />
            </View>
            
            <View style={Style.SectionStyle}>
              <Icon style={Style.icon} name="phone" size={20}/>
              <TextInput
                placeholder="Número de teléfono"
                returnKeyType="go"
                maxLength={10}
                keyboardType="number-pad"
                style={Style.input}
                onChangeText={(phoneNumber) => 
                  {
                    this.setState({phoneNumber})}
                  }
                ref={(input) => { this.phoneNumberInput = input }}
                />
            </View>
            
            <TouchableOpacity
              onPress={ () => this.register() }
              style={Style.button}>
              <Text style={Style.textButton}>Registrate</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={ () => navigate('Login')}>
              <Text style={Style.text}>¿Ya tienes una cuenta?</Text>
              <Text style={[Style.text, { marginTop: -5, fontWeight: 'bold' }]}>Inicia sesión</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    )
  }

  register = () => {
    let { navigate } = this.props.navigation;
    let { email, username, password, phoneNumber, retypePassword } = this.state
    let that = this

    if (email !== '' && username !== '' && password !== '' && retypePassword !== '' && phoneNumber !==  '') {
      that.setState({loading:true})
      if (password === retypePassword) {
        axios.post(`${URL}/add-user`, {
          email:        email,
          username:     username,
          phoneNumber:  phoneNumber,
          password:     retypePassword
        })
        .then(function (response) {
          that.setState({loading:false})
          navigate('Login')
          Toast.show({
            text: 'Se ha registrado con éxito, por favor inicie sesion.',
            buttonText: 'Ok',
            duration: 3000,
            textStyle: { fontSize: 12},
            buttonStyle: { backgroundColor: '#ad59be'},
            buttonTextStyle: { fontSize: 12 }
          })
        })
        .catch(function (error) {
          that.setState({loading:false})
          Toast.show({
            text: error.message,
            buttonText: 'Ok',
            duration: 3000,
            textStyle: { fontSize: 12},
            buttonStyle: { backgroundColor: '#ad59be'},
            buttonTextStyle: { fontSize: 12 }
          })
        });
      }
      else{
        this.setState({loading:false})
        Toast.show({
          text: 'Las contraseñas no coinciden, por favor verificalas',
          buttonText: 'Ok',
          duration: 3000,
          textStyle: { fontSize: 12},
          buttonStyle: { backgroundColor: '#ad59be'},
          buttonTextStyle: { fontSize: 12 }
        })
      }
    }
  }
}
