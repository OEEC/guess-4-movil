import { StyleSheet } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'

export default StyleSheet.create({
  container: {
    padding: 20,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  card: {
    padding: 15,
    paddingTop: 25,
    borderRadius: 10,
    backgroundColor: '#FAFAFA',
  },
  text: {
    marginTop: 20,
    textAlign: 'center',
    color: '#572364',
    margin: 5
  },
  button: {
    borderRadius: 40,
    backgroundColor: '#a903bd',
    marginTop: 25
  },
  textButton: {
    marginStart: 85,
    marginEnd: 85,
    margin: 12,
    fontSize: 14,
    color: '#ffffff',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  logo: {
    width: wp('80%'),
    height: hp('30%')
  },
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    margin: 10,
    borderWidth: 1,
    borderColor: '#d6d5da',
    borderRadius: 80,
    fontSize: 16,
    height: 45,
    color: '#532b64',
    fontWeight: 'bold',
    marginBottom: 5
  },
  icon:{
    color: '#532b64',
    flex: 1,
    padding: 10,
    width: 10
  },
  input:{
    color: '#532b64',
    fontWeight: 'bold',
    flex: 6
  },
  spinnerTextStyle: {
    color: '#FFF'
  },
})
