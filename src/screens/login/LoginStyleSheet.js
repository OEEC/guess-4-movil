import { StyleSheet } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen'

export default StyleSheet.create({
  container: {
    padding: 20,
    flex: 1,
    textAlign: 'center',
    justifyContent: 'center'
  },
  card: { 
    paddingTop: 25,
    padding: 15,
    borderRadius: 10,
    backgroundColor: '#FAFAFA',
    textAlign: 'center'
  },
  text: {
    marginTop: 20,
    textAlign: 'center',
    color: '#572364',
    margin: 5
  },
  button: {
    borderRadius: 40,
    backgroundColor: '#a903bd',
    marginTop: 25
  },
  textButton: {
    marginStart: 85,
    marginEnd: 85,
    margin: 12,
    fontSize: 14,
    color: '#ffffff',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  buttonLogin: {
    marginLeft: 85
  },
  boxContainerLogo: {
    flex: 3,
    flexDirection: 'row',
    alignItems: 'flex-end'
  },
  logo: {
    width: wp('80%'),
    height: hp('30%')
  },
  SectionStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    margin: 10,
    borderWidth: 1,
    borderColor: '#d6d5da',
    borderRadius: 80,
    fontSize: 16,
    height: 45,
    color: '#532b64',
    fontWeight: 'bold',
    marginBottom: 5
  },
  icon:{
    flex: 1,
    color: '#532b64',
    padding: 10,
    width: 10
  },
  input:{
    flex: 6,
    color: '#532b64',
    fontWeight: 'bold'
  },
  spinnerTextStyle: {
    color: '#FFF'
  },
})
