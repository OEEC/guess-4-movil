import React, { Component } from 'react'
import { View, Text, ImageBackground, TextInput, TouchableOpacity, Image, AsyncStorage } from 'react-native'
import { Toast, Button }  from 'native-base'
import { URL }    from '../../config/env'
import Style      from './LoginStyleSheet';
import axios      from 'axios'
import Icon       from 'react-native-vector-icons/FontAwesome'
import Spinner    from 'react-native-loading-spinner-overlay'

export default class LoginScreen extends Component {

  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      loading: false
    }
  }

  componentDidMount(){
    this.user()
  }

  user = async () => {
    let {navigate} = this.props.navigation;
    var t = await AsyncStorage.getItem('token')
    
    if(t != null){
      navigate('Home')
    }
  }

  render() {
    let {navigate} = this.props.navigation;
    let { loading } = this.state;
    return (
      <ImageBackground source={require('../../assets/images/fondo-min.png')} style={{width: '100%', height: '100%'}} >
        <Spinner
          visible={this.state.loading}
          textContent={'Espera!!!...'}
          textStyle={Style.spinnerTextStyle}
        />
        <View style={Style.container}>
          <View style={Style.card}>
            <Image source={require('../../assets/images/guess4-logo-1024-min.png')} style={Style.logo} ></Image>
            <View style={Style.SectionStyle}>
              <Icon style={Style.icon} name="envelope" size={20}/>
              <TextInput
                placeholder="Correo eléctronico"
                returnKeyType="next"
                autoCapitalize={'none'}
                onSubmitEditing = {() => this.passwordInput.focus() }
                onChangeText={(email) => { this.setState({email: email})}}
                style={Style.input}
                keyboardType="email-address"/>
            </View>
            
            <View style={Style.SectionStyle}>
              <Icon style={Style.icon} name="key" size={20}/>
              <TextInput
              placeholder="Contraseña"
              returnKeyType="go"
              ref={(input) => { this.passwordInput = input }}
              style={Style.input}
              onChangeText={ (password) => { this.setState({password:  password}) }}
              secureTextEntry/>
            </View>
            <TouchableOpacity
                onPress={ () => this.login() }
                style={Style.button}> 
                <Text style={Style.textButton}>Iniciar sesión</Text>
              </TouchableOpacity> 
            <TouchableOpacity
              onPress={() => navigate('Register')}>
              <Text style={Style.text}>¿No tienes cuenta?</Text>
              <Text style={[Style.text, { marginTop: -5, fontWeight: 'bold' }]}>Registrate</Text>
            </TouchableOpacity>
            {/* <View style={Style.buttonLogin}>
              <LoginButton
                async onLoginFinished={
                  (error, result) => {
                    if (error) {
                      Toast.show({
                        text: error,
                        buttonText: 'Ok',
                        duration: 3000,
                        textStyle: { fontSize: 12},
                        buttonStyle: { backgroundColor: '#ad59be'},
                        buttonTextStyle: { fontSize: 12 }
                      })
                    } else if (result.isCancelled) {
                      Toast.show({
                        text: 'No se ha podido registrar, vuelva a intentarlo mas tarde',
                        buttonText: 'Ok',
                        duration: 3000,
                        textStyle: { fontSize: 12},
                        buttonStyle: { backgroundColor: '#ad59be'},
                        buttonTextStyle: { fontSize: 12 }
                      })
                    } else {
                      AccessToken.getCurrentAccessToken().then(
                        (data) => {
                          AsyncStorage.setItem('user', 'Exist');
                          navigate('Home')
                        }
                      )
                    }
                  }
                }
                onLogoutFinished={() => console.log("logout.")}/>
            </View> */}
          </View>
        </View>
      </ImageBackground>
    )
  }

  login = () => {
    let { navigate }        = this.props.navigation;
    let { email, password } = this.state;
    let that                = this;
    
    if ( email !== '' && password !== '') {
      this.setState({ loading:true })
      // http://192.168.2.1:8000/login-app
      // `${URL}/login-app`
      axios.post(`${URL}/login-app`, {
        email: this.state.email,
        password: this.state.password
      })
      .then(async function (response) {
        if(response.data.success){
          await AsyncStorage.setItem('id',    ''+response.data.id);
          await AsyncStorage.setItem('user',  response.data.user);
          await AsyncStorage.setItem('email', ''+response.data.email);
          await AsyncStorage.setItem('phone', ''+response.data.phone);
          await AsyncStorage.setItem('photo', response.data.image)
          await AsyncStorage.setItem('token', response.data.token);
          that.setState({ loading:false })
          
          navigate('Home')
        }
        else{
          Toast.show({
            text: response.data.message,
            buttonText: 'Ok',
            duration: 9000,
            textStyle: { fontSize: 12},
            buttonStyle: { backgroundColor: '#ad59be'},
            buttonTextStyle: { fontSize: 12 }
          })
          that.setState({ loading: false })
        }
      })
      .catch(function (error) {
        console.log(error)
      });
    }
    else{
      Toast.show({
        text: 'Debe llenar todos los campos.',
        buttonText: 'Ok',
        duration: 3000,
        textStyle: { fontSize: 12},
        buttonStyle: { backgroundColor: '#ad59be'},
        buttonTextStyle: { fontSize: 12 }
      })
    }
  }
}
