import React, { Component } from 'react'
import { View, Text, ImageBackground, Image } from 'react-native'
import { Container, Content } from 'native-base'
import Header from '../../components/HeaderComponent'
import style from './InstructionsStyleSheet'

export default class InstructionsScreen extends Component {

  static navigationOptions = {
    drawerLabel: 'Instrucciones'
  }

  constructor(props) {
    super(props)
  
    this.state = {
       
    }
  }
  
  render() {
    let { navigation } = this.props
    return (
      <Container>
        <ImageBackground source={require('../../assets/images/fondo-min.png')} style={{width: '100%', height: '100%'}}>
          <Header navigation={navigation}/>
          <Content>
            <View style={style.container}>
              <Text style={style.text}>Instrucciones</Text>
              <View style={style.card}>
                <View style={style.boxInstruction}>
                  <View style={style.instruction}>
                    <Text style={style.textNumber}>1</Text>
                    <Text style={style.textInstruction}>Open an account</Text>
                  </View>
                  <View style={style.instruction}>
                    <Text style={style.textNumber}>2</Text>
                    <Text style={style.textInstruction}>Open an account</Text>
                  </View>
                  <View style={style.instruction}>
                    <Text style={style.textNumber}>3</Text>
                    <Text style={style.textInstruction}>Open an account</Text>
                  </View>
                  <View style={style.instruction}>
                    <Text style={style.textNumber}>4</Text>
                    <Text style={style.textInstruction}>Open an account</Text>
                  </View>
                </View>
              </View>
            </View>
          </Content>
        </ImageBackground>
      </Container>
    )
  }
}

