import { StyleSheet } from 'react-native'

export default StyleSheet.create ({
  container: {
    flex: 1,
    padding: 20
  },
  card: {
    backgroundColor: '#FFF',
    padding: 12,
    borderRadius: 10
  },
  text: {
    color: '#FFF',
    marginBottom: '2%',
    fontWeight: 'bold'
  },
  boxInstruction: {
    flex: 1,
    justifyContent: 'space-around'
  },
  instruction: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 100,
    padding: 5
  },
  textNumber: {
    fontSize: 8,
    borderRadius: 100,
    padding: 4,
    textAlign: 'center',
    backgroundColor: '#ad59be',
    color: '#FFF',
    width: 18
  },
  textInstruction: {
    marginLeft: 5,
    fontWeight: 'bold',
    color: '#b2b2b2'
  }
})
