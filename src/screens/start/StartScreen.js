import React, { Component } from 'react';
import { View, TouchableOpacity, Image, ImageBackground, AsyncStorage } from 'react-native';
import { Button, Text } from 'native-base';
import Style from './StartStyleSheet';

export default class StartScreen extends Component {
  constructor(props) {
    super(props)
  }

  async next(){
    let { navigate } = this.props.navigation
    let user = await AsyncStorage.getItem('token')
    
    if(user != null){
      navigate('Home')
    }
    else{
      navigate('Login')
    }
  }

  render() {
    return (
      <ImageBackground source={require('../../assets/images/fondo-min.png')} style={Style.container}>
        <View style={Style.viewContainer}>
          <View style={Style.boxContainerLogo}>
            <Image source={require('../../assets/images/guess4-logo-1024-min.png')} style={Style.logo} ></Image>
          </View>
          <View style={Style.boxContainerText}>
            <Text style={Style.text}>Juego interactivo</Text>
            <Text style={[Style.text, {marginTop: -1}]}>de trivia en vivo</Text>
          </View>
          <View style={Style.boxContainerButton}>
            <TouchableOpacity onPress={this.next.bind(this)} style={Style.button}>
              <Text style={Style.textButton}>Comencemos</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    )
  }
}
