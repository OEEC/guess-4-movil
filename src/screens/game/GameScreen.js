import React, { Component } from 'react'
import { View, Image, Text, ImageBackground, FlatList, TouchableOpacity, AsyncStorage, ScrollView } from 'react-native'
import style    from './GameStyleSheet'
import { Icon, Toast, Button, Header, Left, Body, Right, } from 'native-base'
import axios    from 'axios'
import { URL, URLalter }  from '../../config/env'
import Spinner  from 'react-native-loading-spinner-overlay'

export default class GameScreen extends Component {

  constructor(props) {
    super(props)
    this.state = {
      selectedBrands: this.props.navigation.getParam('selectedBrands'),
      brands:         [],
      isVisible:      false,
      nextScreen:     false,
      loading:        false,
    
    }
  }

  componentDidMount() {
    this.getPublicity()
  }

  componentDidUpdate() {
    let { navigate } = this.props.navigation

    if (this.state.nextScreen) {
      navigate('Ad', { selectedBrands: this.state.selectedBrands })
      this.setState({ nextScreen: false })
    }
  }

  render() {
    let { navigation } = this.props
    return (
      <ScrollView >
      <ImageBackground source={require('../../assets/images/fondo-min.png')} 
      style={{ width: '100%', height: '100%' }}>
        <Header transparent androidStatusBarColor="#532b64" iosBarStyle="light-content">
          <Left>
            <TouchableOpacity onPress={() => {
              navigation.navigate('Home')
            }}>
              <Icon name="arrow-back" style={style.arrowBack}></Icon>
            </TouchableOpacity>
          </Left>
          <Body>
          </Body>
          <Right>
          </Right>
        </Header>
        <Spinner
          visible={this.state.loading}
          textContent={'Espera!!!...'}
          textStyle={style.spinnerTextStyle}
        />
        <View style={style.container}>
          <View style={style.selectedBrands}>
            <FlatList
              data={this.state.selectedBrands}
              renderItem={({ item }) => {
                 if (item.state) {
                    return (
                      <View style={style.boxSelectedBrands}>
                        <View style={style.buttonDeleteBrand}>
                            <Icon name="close" style={style.iconDelete} />
                          </View>
                        <TouchableOpacity onPress={() => this.removeBrand(item)} style={style.borderImage}>
                          <Image style={style.imageBrand} resizeMode="contain" 
                          source={{ uri: `${URL}/assets/images/brands/${item.src}`}} />
                        </TouchableOpacity>
                      </View>
                    )
                  } else {
                    return (
                      <View style={style.boxSelectedBrands}>
                        <View style={style.boxEmpty}>
                          <Text style={style.textBoxEmpty}>GUESS4</Text>
                        </View>
                      </View>)
                  }
                
                 }
               
              
              }
              keyExtractor={(value, index) => index.toString()}
              numColumns={4}
            />
          </View>
          <View style={style.imagesBrands}>
            <FlatList
              data={this.state.brands}
              renderItem={({ item }) =>
                <TouchableOpacity
                  style={style.touchableBrand}
                  onPress={() => { this.selectBrand(item) }}>
                  <Image style={style.image} source={{ uri: `${URL}/assets/images/brands/${item.src}` }} />
                </TouchableOpacity>
              }
              keyExtractor={(value, index) => index.toString()}
              numColumns={2}>
            </FlatList>
          </View>
          {/* <View style={style.contentPrize}>
            <View>
              <Text style={style.textPrize}>Premio</Text>
              <Text style={style.textMoney}>${this.state.money}</Text>
            </View>
          </View> */}
          { this.state.selectedBrands[3].state ? this.nextButton() : <View/> }
        </View>
      </ImageBackground>
      </ScrollView>
    )
  }

  nextButton() {
    return (
      <Button rounded small block iconRight style={{ backgroundColor: '#a903bd', height: 40 }}
        onPress={this.endGame.bind(this)}>
        <Text style={{ color: '#FFF', textAlignVertical: 'center', marginBottom: '1%', marginTop: 3 }}>Finalizar</Text>
        <Icon name="chevron-right" type='FontAwesome' style={{ color: '#FFF', fontSize: 16, marginTop: 3 }} />
      </Button>
    )
  }

  selectBrand(item) {
    let array = this.state.selectedBrands

    for (let i = 0; i < array.length; i++) {
      if (array[i].state === false) {
        array[i] = item
        this.props.navigation.push('Ad', { selectedBrands: array, id:this.props.navigation.getParam('id'), 
        video:array[i].video})
        console.log('item')
        console.log(item)
        console.log('array')
       console.log(array)
       console.log('selectedBrands')
       console.log(this.state.selectedBrands)
     
        return
      }
    }
    return
   }
  
  removeBrand(item) {
    let array = this.state.selectedBrands
    let index = array.indexOf(item)
    this.state.selectedBrands.splice(index, 1)
    this.state.selectedBrands.push({ id: null, src: null, video: null, state: false, add: false })
    this.setState({ selectBrands: array })
    this.setState({ nextScreen: false })
  }

  getPublicity() {
    const that = this
    const brands = []

    axios.get(`${URL}/get-publicity`)
      .then(function (response) {
        response.data.map(function (element) {
          const array = { 'id': element.id, 'src': element.image, 'video': element.video, 
          'state': true, add: false }
          brands.push(array)
          that.setState({ brands: brands })
        })
      })
      .catch(function (error) {
        Toast.show({
          text: error.message,
          buttonText: 'Ok',
          duration: 3000,
          textStyle: { fontSize: 12 },
          buttonStyle: { backgroundColor: '#ad59be' },
          buttonTextStyle: { fontSize: 12 }
        })
      });
  }

  endGame = async () => {
    let that = this
    
    const { selectedBrands } = this.state
    const id    = await AsyncStorage.getItem('id')
    const code  = this.props.navigation.getParam('id')
    const codes = { player:id, idCode:code, combination:selectedBrands }
    
    axios({
      method:'POST',
      url: `${URL}/generate-code`,
      headers: {
        'Content-Type': 'application/json'
      },
      data: codes
    })
    .then(function (response) {
      let scope = that
      console.log(response);
      
      if (response.data.success) {
        console.log('Si se hace');
        
        scope.props.navigation.navigate('Congratulation')
      }
      else {
        console.log('hi');
        
        console.log(response);
        
        // Toast.show({
        //   text: response.data.message,
        //   buttonText: 'Ok',
        //   duration: 8000,
        //   textStyle: { fontSize: 12 },
        //   buttonStyle: { backgroundColor: '#ad59be' },
        //   buttonTextStyle: { fontSize: 12 }
        // })
        scope.props.navigation.navigate('Congratulation')
      }
    })
    .catch(function (error) {
      console.log('Error: '+error);
      
      Toast.show({
        text: error.message,
        buttonText: 'Ok',
        duration: 8000,
        textStyle: { fontSize: 12 },
        buttonStyle: { backgroundColor: '#ad59be' },
        buttonTextStyle: { fontSize: 12 }
      })
    });
  }
}
