import { StyleSheet } from 'react-native'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen'

export default StyleSheet.create({
  container: {
    padding: 20,
    position: 'relative'
  },
  imagesBrands: {
    margin: 10,
    backgroundColor: '#FFFFFF',
    padding: 15,
    borderRadius: 10,
    height: hp('70%'),
  },
  contentPrize: {
    margin: 30,
    backgroundColor: '#FFFFFF',
    position: 'absolute',
    padding: 15,
    borderRadius: 10,
    flexDirection: "row",
    bottom: -100,
    left: 0,
    right: 0,
    justifyContent: 'space-between'
  },
  textPrize: {
    fontWeight: 'bold',
    fontSize: 18,
    color: '#532b64',
  },
  textPrizeResult: {
    right: 0,
    left: 60,
    fontWeight: 'bold',
    fontSize: 18,
    color: '#532b64',
  },
  textMoney: {
    fontWeight: 'bold',
    fontSize: 18,
    color: '#ad59be'
  },
  textDate: {
    fontWeight: 'bold',
    fontSize: 15,
    color: '#532b64'
  },
  boxSelectedBrands: {
    flex: 1,
    alignItems: 'center'
  },
  boxEmpty: {
    width: 65,
    height: 65,
    borderRadius: 100,
    backgroundColor: '#FAFAFA',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textBoxEmpty: {
    fontSize: 14,
    fontWeight: '900',
    color: '#b2b2b2',
    fontSize: 10,
    transform: [{scaleY: 2}]
  },
  boxIcon: {
    position: 'absolute',
    right: 0,
    backgroundColor: '#c8c7c7',
    borderRadius: 100,
    width: 20,
    height: 20,
    alignItems: 'center',
    padding: 5,
    justifyContent: 'center',
    flex: 1
  },
  icon: {
    color: '#FAFAFA',
    fontWeight: 'bold',
    fontSize: 14
  },
  touchableBrand: {
    flex: 1,
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#B7B7B7',
    borderStyle: 'dotted'
  },
  image: {
    width: wp('20%'),
    height: hp('15%'),
    resizeMode: 'contain',
  },
  imageBrand: {
    flex:1,
    width:60,
    height:60
  },
  borderImage:{
    width: 70,
    height: 70,
    padding:5,
    borderRadius: 100,
    overflow: 'hidden',
    backgroundColor: '#ffffff',
    flexDirection:"row",
    justifyContent:"center",
    alignItems:"center"
  },
  buttonDeleteBrand: {
    position: 'absolute',
    right: 0,
    backgroundColor: '#c5c5c5',
    borderRadius: 100, 
    width: 20,
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  iconDelete: {
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 20
  },
  arrowBack: {
    color: '#FFF',
    padding: 10
  },
})
