import React, { Component } from 'react'
import { View, Text, ImageBackground, FlatList, TouchableOpacity, Image, BackHandler } from 'react-native'
import { Icon, Button, Toast } from 'native-base'
import {URL}            from '../../config/env'
import style            from './AdStyleSheet'
import VideoPlayer      from 'react-native-video-controls'
import Video            from 'react-native-video'


export default class AdScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      brand: this.props.navigation.state.params.selectedBrands,
      video: this.props.navigation.state.params.video,
      brands: [],
      beforebrands: this.props.navigation.state.params.beforebrand,
      finishAd: false,
      vali: false,
       
    }
    
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handlerBackButton )
    let that = this
    setInterval(function timer() {
      console.log('algo');
      that.setState({ vali:true })
    }, 3000);
    // store intervalId in the state so it can be accessed later:
   // this.setState({intervalId: intervalId});  
  }

  handlerBackButton = () => {
    return true
  }

  render() {
    
    const {vali} = this.state
    return (
      <ImageBackground source={require('../../assets/images/fondo-min.png')} 
      style={{width: '100%', height: '100%'}}>
        <View style={style.container}>
          <View style={style.selectedBrands}>
            <FlatList
              data={this.state.brand}
              renderItem={({item}) => {
                if (item.state) {
                  return(
                    // onPress={this.removeBrand(item)}
                  <View style={style.boxSelectedBrands}>
                    <TouchableOpacity style={style.borderImage}>
                      <Image style={style.imageBrand} resizeMode="contain" 
                      source={{ uri:`${URL}/assets/images/brands/${item.src}` }} />
                    </TouchableOpacity>
                  </View>
                )
                } else {
                  return(
                    <View style={style.boxSelectedBrands}>
                      <View style={style.boxEmpty}>
                        <Text style={style.textBoxEmpty}>GUESS4</Text>
                      </View>
                    </View>)
                }
              }
              }
              keyExtractor={(value, index) => index.toString()}
              numColumns={4}
            />
          </View>
          <View style={style.imagesBrands}>
             <Video source={{ uri: `${URL}/assets/videos/brands/${this.state.video}`}}   // Can be a URL or a localfile. */}
              
              onEnd={ () => this.checkFinishAd(this)}                      // Callback when playback finishes
              onError={ () => this.videoError(this)}               // Callback when video cannot be loaded
              style={style.backgroundVideo}/>
            
                {
                  this.state.vali ?
                  <TouchableOpacity 
                    style={style.borderImage} onPress={() => { this.checkFinishAd(this)}}>
                     <Text>Saltar</Text>
                  </TouchableOpacity> :
                  <View/>
                }
          </View>
        </View>
      </ImageBackground>
    )
  }

  videoError = (props) =>{
    let {push} = this.props.navigation
    
    Toast.show({
      text: 'No se ha encontrado el video',
      buttonText: 'Ok',
      duration: 3000,
      textStyle: { fontSize: 12 },
      buttonStyle: { backgroundColor: '#ad59be' },
      buttonTextStyle: { fontSize: 12 }
    })
    console.log('video Errors')
    console.log(this.state.brand)
   // push('Game', {selectedBrands: this.state.beforebrands})
  
  }

  checkFinishAd = (props) => {
    let that = this
    const { push } = this.props.navigation

    push('Game', {selectedBrands: that.state.brand})
 
    console.log('checkfinshedad')
    console.log(that.state.brand)

  }
  
  // myFunction() {
  //   this.timer = setInterval( this.state.vali=true, 3000);
  // }

  // removeBrand() {
  //   const that                = this
  //   const { selectedBrands }  = this.state
  //   let array                 = selectedBrands
  //   // let index                 = array.indexOf(item)
  //   let index                 = 1

  //   this.state.selectedBrands.splice(1)
  //   selectedBrands.push({ id: null, src: null, video: null, state: false, add: false })
  //   this.setState({selectBrands: array})
  //   this.props.navigation.push('Game', {selectedBrands: that.state.selectedBrands})
  // }
}
