import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  },
  selectedBrands: {
    margin: 25
  },
  boxSelectedBrands: {
    flex: 1,
    alignItems: 'center'
  },
  imageBrand: {
    flex:1,
    width:60,
    height:60
  },
  buttonDeleteBrand: {
    position: 'absolute',
    right: 0,
    backgroundColor: '#c8c7c7',
    borderRadius: 100,
    overflow: 'hidden',
    width: 20,
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  iconDelete: {
    color: '#FAFAFA',
    fontWeight: 'bold',
    fontSize: 14
  },
  contentPrize: {
    margin: 10,
    marginTop: 35,
    backgroundColor: '#FFFFFF',
    padding: 15,
    borderRadius: 5,
    overflow: 'hidden',
    flexDirection: "row",
    left: 0,
    right: 0,
    justifyContent: 'space-between'
  },
  textPrize: {
    fontWeight: 'bold',
    fontSize: 18,
    color: '#532b64',
  },
  textPrizeResult: {
    right: 0,
    left: 60,
    fontWeight: 'bold',
    fontSize: 18,
    color: '#532b64',
  },
  textMoney: {
    fontWeight: 'bold',
    fontSize: 18,
    color: '#ad59be'
  },
  textDate: {
    fontWeight: 'bold',
    fontSize: 15,
    color: '#532b64'
  },
  button: {
    position: 'absolute',
    borderRadius: 40,
    overflow: 'hidden',
    backgroundColor: '#ad59be',
    alignContent: 'center',
    left: 190,
    right: 8,
    top: 312,
    bottom: 8
  },
  textButton: {
    textAlign: 'center',
    marginStart: 10,
    marginEnd: 10,
    margin: 7,
    fontSize: 12,
    color: '#fafafa',
    fontWeight: 'bold',
  },
  boxEmpty: {
    width: 65,
    height: 65,
    borderRadius: 100,
    backgroundColor: '#FAFAFA',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textBoxEmpty: {
    fontSize: 14,
    fontWeight: '900',
    color: '#b2b2b2',
    fontSize: 10,
    transform: [{scaleY: 2}]
  },
  borderImage:{
    width: 70,
    height: 70,
    padding:5,
    borderRadius: 100,
    overflow: 'hidden',
    backgroundColor: '#ffffff',
    flexDirection:"row",
    justifyContent:"center",
    alignItems:"center"
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: '#000',
    height: 600
  },
})
