import React, { Component } from 'react'
import { ImageBackground, View, TextInput, Text, TouchableOpacity } from 'react-native'
import { Toast } from 'native-base'
import style from './ConfirmCodeStyleSheet'

export default class ConfirmCodeScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      confirmCode: '',
      code: '',
      credential: null
    }
    // this.state = {
    //   timer: null,
    //   counter: 30
    // }
  }

  componentDidMount() {
    let {navigate} = this.props.navigation;

    // firebase.auth().verifyPhoneNumber('+52' + this.props.navigation.state.params.phoneNumber)
    //   .on('state_changed', (phoneAuthSnapshot) => {
    //       switch (phoneAuthSnapshot.state) {
    //         case firebase.auth.PhoneAuthState.CODE_SENT:
    //           Toast.show({
    //             text: 'El codigo ha sido enviado',
    //             buttonText: 'Ok',
    //             duration: 3000,
    //             textStyle: { fontSize: 12},
    //             buttonStyle: { backgroundColor: '#ad59be'},
    //             buttonTextStyle: { }
    //           })
    //           break;
    //         case firebase.auth.PhoneAuthState.ERROR:
    //           Toast.show({
    //             text: 'A ocurrido un error',
    //             buttonText: 'Ok',
    //             duration: 3000,
    //             textStyle: { fontSize: 12},
    //             buttonStyle: { backgroundColor: '#ad59be'},
    //             buttonTextStyle: { }
    //           })
    //           break;
    //         case firebase.auth.PhoneAuthState.AUTO_VERIFIED:
    //           Toast.show({
    //             text: 'Se verifico el codigo de confirmación',
    //             buttonText: 'Ok',
    //             duration: 3000,
    //             textStyle: { fontSize: 12},
    //             buttonStyle: { backgroundColor: '#ad59be'},
    //             buttonTextStyle: { }
    //           })
    //           navigate('DrawerStack')
    //           break;
    //       }
    //     }, (error) => {
    //       // console.warn(error);
    //       // console.warn(error.verificationId);
    //     }, (phoneAuthSnapshot) => {
    //       // console.warn(phoneAuthSnapshot);
    //     });
  }

  render() {
    return(
      <ImageBackground source={require('../../assets/images/fondo-min.png')} style={{width: '100%', height: '100%'}} >
        <View style={style.container}>
          <View style={style.card}>
            <Text style={style.text}>Codigo de confirmación</Text>
            <TextInput
              style={style.input}
              placeholder="Codigo"
              keyboardType="numeric"
              onChangeText={ (code) => this.setState({code})}
            />
            <TouchableOpacity style={style.button} onPress={this.verifyCode}>
              <Text style={style.textButton}>Send</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    )
  }

  verifyCode = () => {

    this.state.confirmResult.confirm(this.state.code)
      .then(user => console.warn(user))
      .catch(err => console.warn(err))
  }
}
