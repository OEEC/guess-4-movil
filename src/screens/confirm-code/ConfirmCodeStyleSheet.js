import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 20
  },
  card: {
    alignItems: 'center',
    padding: 20,
  },
  text: {
    color: '#FFF',
    fontSize: 18,
    marginBottom: '5%',
    fontWeight: 'bold'
  },
  input: {
    backgroundColor: '#FFF',
    borderRadius: 4,
    color: '#532b64',
    fontWeight: 'bold',
    width: '100%',
    textAlign: 'center'
  },
  button: {
    margin: '8%',
    borderRadius: 40,
    padding: 8,
    backgroundColor: '#ad59be'
  },
  textButton: {
    color: '#FFF',
    fontWeight: 'bold',
    fontSize: 14,
    marginStart: '7%',
    marginEnd: '7%'
  }
})
