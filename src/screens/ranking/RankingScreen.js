import React, { Component } from 'react'
import { ImageBackground, View, Image, Text, FlatList } from 'react-native'
import { Container, Content, Badge } from 'native-base'
import { Col, Row, Grid } from "react-native-easy-grid"
import Header from '../../components/HeaderComponent'
import style from './RankingStyleSheet'

export default class RankingScreen extends Component {

  static navigationOptions = {
    drawerLabel: 'Top de usuarios'
  }

  constructor(props) {
    super(props)
  
    this.state = {
       data: [
         {user: 'User20', won: 6},
         {user: 'User20', won: 6},
         {user: 'FireProof', won: 6},
         {user: 'User20', won: 6},
         {user: 'Use20', won: 3},
         {user: 'User20', won: 4},
         {user: 'User20', won: 4},
         {user: 'User20', won: 4},
         {user: 'User20', won: 4},
         {user: 'Use0', won: 2},
         {user: 'User20', won: 4},
         {user: 'User20', won: 3}
       ]
    }
  }
  
  render() {
    let { navigation } = this.props;
    return (
      <Container>
        <ImageBackground source={require('../../assets/images/fondo-min.png')} style={{width: '100%', height: '100%'}}>
          <Header navigation={navigation}/>
          <Content style={style.container}>
            <Text style={style.text}>Top 3</Text>
            <View style={style.cardTop3}>
              <View style={style.boxUserTop}>
                <Text style={style.textNumberRanking}>1</Text>
                <Image source={require('../../assets/images/fondo-min.png')} style={style.image}/>
                <Text style={style.textRanking}>User20</Text>
                <Text style={style.textRanking}>Won 7</Text>
              </View>
              <View style={style.boxUserTop}>
                <Text style={style.textNumberRanking}>2</Text>
                <Image source={require('../../assets/images/fondo-min.png')} style={style.image}/>
                <Text style={style.textRanking}>User20</Text>
                <Text style={style.textRanking}>Won 7</Text>
              </View>
              <View style={style.boxUserTop}>
                <Text style={style.textNumberRanking}>3</Text>
                <Image source={require('../../assets/images/fondo-min.png')} style={style.image}/>
                <Text style={style.textRanking}>User20</Text>
                <Text style={style.textRanking}>Won 7</Text> 
              </View>
            </View>
            <Text style={style.text}>Ranking</Text>
            <View style={style.cardRanking}>
              <View style={style.boxInstruction}>
                <Grid>
                  <FlatList 
                    data={this.state.data}
                    renderItem={ ({item, index}) =>
                      <Row style={style.instruction}>
                        <Col size={10}>
                          <Text style={style.textNumber}>{ index + 1 }</Text>
                        </Col>
                        <Col size={70}>
                          <Text style={style.textInstruction}>{item.user}</Text>
                        </Col>
                        <Col size={20}>
                          <Text style={style.textInstruction2}>Won
                            <Text style={style.textNumberRanking}> {item.won}</Text>
                          </Text>
                        </Col>
                      </Row>
                    }
                    keyExtractor={(value, index) => index.toString() }
                  />
                </Grid>
              </View>
            </View>
          </Content>
        </ImageBackground>
      </Container>
    )
  }
}
