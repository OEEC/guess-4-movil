import { StyleSheet } from 'react-native'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen'

export default StyleSheet.create({
  container: {
    padding:  20
  },
  cardTop3: {
    backgroundColor: '#FFF',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 20,
    borderRadius: 10
  },
  boxUserTop: {
    flex: 1,
    alignItems: 'center'
  },
  image: {
    borderRadius: 100,
    width: 75,
    height: 75
  },
  text: {
    color: '#FFF',
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: '3%',
    marginTop: '2.4%'
  },
  cardRanking: {
    backgroundColor: '#FFF',
    flex: 1,
    padding: 20,
    borderRadius: 10,
  },
  textRanking: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#b2b2b2'
  },
  textNumberRanking: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'rgb(122, 0, 129)'
  },
  boxInstruction: {
    flex: 1,
    justifyContent: 'space-around',
    height: hp('36%')
  },
  instruction: {
    flex: 1,
    flexDirection: 'row',
    padding: 5,
    borderBottomWidth: 1,
    borderBottomColor: '#B7B7B7'
  },
  textNumber: {
    fontSize: 8,
    borderRadius: 100,
    padding: 4,
    textAlign: 'center',
    backgroundColor: '#ad59be',
    color: '#FFF',
    width: 18,
  },
  textInstruction: {
    fontWeight: 'bold',
    color: '#b2b2b2'
  },
  textInstruction2: {
    fontWeight: 'bold',
    textAlign: 'right',
    color: '#b2b2b2'
  }
})
