import { StyleSheet, Dimensions, Platform9 } from 'react-native'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen'

let { width, height } = Dimensions.get('window')

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  },
  card: {
    backgroundColor: '#FFF',
    borderRadius: 10,
    padding: 20,
    alignItems: 'center'
  },
  boxPhotoProfile: {
    flex: 1,
    alignItems: 'center',
    marginBottom: '8%'
  },
  textChangePhoto: {
    position: 'absolute',
    top: '50%',
    width: width/4.5,
    height: height/20,
    bottom: 0,
    backgroundColor: '#000',
    opacity: 0.5,
    alignItems: 'center',
    borderBottomRightRadius: 100,
    borderBottomLeftRadius: 100
  },
  input: {
    borderWidth: 1,
    borderColor: '#d6d5da',
    borderRadius: 80,
    fontSize: 16,
    height: 45,
    color: '#532b64',
    fontWeight: 'bold',
    marginBottom: 5,
    paddingHorizontal: 20,
    width: '80%'
  },
  button: {
    borderRadius: 40,
    backgroundColor: '#ad59be',
    marginTop: 15
  },
  buttonDisable: {
    borderRadius: 40,
    backgroundColor: '#ad59be',
    marginTop: 15,
    opacity: 0.5
  },
  textButton: {
    marginStart: 40,
    marginEnd: 40,
    margin: 12,
    fontSize: 14,
    color: '#ffffff',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  arrowBack: {
    color: '#FFF',
    padding: 10
  },
  textHeader: {
    color: '#FFF',
    fontSize: wp('5%')
  }
})
