import React, { Component } from 'react'
import { View, Text, ImageBackground, TextInput, TouchableOpacity, AsyncStorage } from 'react-native'
import { Container, Content, Header, Left, Icon, Body, Right, Toast, Thumbnail } from 'native-base'
import { URL }      from '../../config/env'
import style        from './EditProfileStyleSheet'
import ImagePicker  from 'react-native-image-picker'
import axios        from 'axios'
import Spinner      from 'react-native-loading-spinner-overlay'

export default class EditProfileScreen extends Component {
  static navigationOptions = {
    drawerLabel: () => null
  }

  constructor(props) {
    super(props)
    this.state = {
      id:           '',
      user:         '',
      username:     '',
      email:        '',
      phoneNumber:  '',
      source:       '',
      photoURL:     '',
      photoSend:    {},
      loading:      false
    }
  }

  componentDidMount() {
    this.getData()
  }

  async getData() {
    let user  = await AsyncStorage.getItem('user')
    let email = await AsyncStorage.getItem('email')
    let phone = await AsyncStorage.getItem('phone')
    let photo = await AsyncStorage.getItem('photo')

    this.setState({username:    user})
    this.setState({email:       email})
    this.setState({phoneNumber: phone})
    this.setState({photoURL: `https://www.guess4.com.mx/assets/images/users/${photo}`})
  }

  render() {
    let { navigation } = this.props
      return(
        <Container>
          <ImageBackground source={require('../../assets/images/fondo-min.png')} style={[{width: '100%', height: '100%'}]}>
            <Header transparent androidStatusBarColor="#532b64" iosBarStyle="light-content">
              <Left>
                <TouchableOpacity onPress={() => {
                  navigation.dispatch('Profile')
                  navigation.navigate('Profile')
                }}>
                  <Icon name="arrow-back" style={style.arrowBack}></Icon>
                </TouchableOpacity>
              </Left>
              <Body>
                <Text style={style.textHeader}>Editar perfil</Text>
              </Body>
              <Right>
              </Right>
            </Header>
            <Content style={style.container}>
              <View style={style.card}>
                <Spinner
                  visible={this.state.loading}
                  textContent={'Guardando...'}
                  textStyle={style.spinnerTextStyle}
                />
                <View style={style.boxPhotoProfile}>
                  <Thumbnail large source={this.state.source ? {uri: this.state.source.uri} : { uri: this.state.photoURL }} />
                  <TouchableOpacity style={style.textChangePhoto} onPress={this.selectImage.bind(this)}>
                    <Text style={{fontSize: 12, color: '#FFF'}}>Cambiar</Text>
                  </TouchableOpacity>
                </View>
                <TextInput
                  placeholder="Nombre de usuario"
                  style={style.input}
                  editable={true}
                  onChangeText={(username) => {
                    this.setState({username})
                  }}
                  value={this.state.username}/>
                <TextInput
                  placeholder="Correo electrónico"
                  style={style.input}
                  onChangeText={(email) =>{
                    this.setState({email})
                  }}
                  value={this.state.email}/>
                <TextInput
                  placeholder="Número de teléfono"
                  style={style.input}
                  keyboardType="number-pad"
                  maxLenght={10}
                  minLenght={10}
                  editable={false}
                  onChangeText={(phoneNumber) =>{
                    this.setState({phoneNumber})
                  }}
                  value={this.state.phoneNumber}/>
                <TouchableOpacity style={style.button} onPress={this.updateUser.bind(this)}>
                  <Text style={style.textButton}>Guardar</Text>
                </TouchableOpacity>
              </View>
            </Content>
          </ImageBackground>
        </Container>
      )
  }

  selectImage() {
     let options = {
       title: '¿Que fotografia quieres?',
     }
     let that = this
     
     ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        Toast.show({
          text: '¡Cancelado!',
          buttonText: 'Ok',
          duration: 3000,
          textStyle: { fontSize: 12},
          buttonStyle: { backgroundColor: '#ad59be'},
          buttonTextStyle: { fontSize: 12 }
        })
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
        Toast.show({
          text: response.error,
          buttonText: 'Ok',
          duration: 3000,
          textStyle: { fontSize: 12 },
          buttonStyle: { backgroundColor: '#ad59be' },
          buttonTextStyle: { fontSize: 12 }
        })
      } 
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        Toast.show({
          text: response.customButton,
          buttonText: 'Ok',
          duration: 3000,
          textStyle: { fontSize: 12 },
          buttonStyle: { backgroundColor: '#ad59be' },
          buttonTextStyle: { fontSize: 12 }
        })
      } 
      else {
        let source  = response
        let file    = {
          uri:  source.uri,
          type: source.type,
          name: source.fileName
        }
        
        that.setState({ photoURL: response.uri })
        that.setState({ photoSend:file })
      }
    });
  }

  async updateUser () {
    let that = this
    let user = {
      id:     await AsyncStorage.getItem('id'),
      email:  this.state.email,
      name:   this.state.username,
      phone:  this.state.phoneNumber,
      file:   this.state.photoSend
    }
    this.setState({ loading:true })

    let formData = new FormData()
    formData.append('name',   this.state.username)
    formData.append('phone',  this.state.phoneNumber)
    formData.append('email',  this.state.email)
    formData.append('id', await AsyncStorage.getItem('id'))
    formData.append('file', this.state.photoSend)

    if(this.state.email != '' && this.state.username != '' && this.state.phoneNumber != ''){
      axios({
        method:'POST',
        // url: `${URL}/edit-user`,
        // 'http://192.168.2.1:8000/edit-user'
        url: `${URL}/edit-user`,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data'
        },
        data: formData
      })
      .then(function (response) {
        if (response.data.success) {
          that.setState({ loading: false })
          Toast.show({
            text: response.data.message,
            buttonText: 'Ok',
            duration: 3000,
            textStyle: { fontSize: 12 },
            buttonStyle: { backgroundColor: '#ad59be' },
            buttonTextStyle: { fontSize: 12 }
          })
          
          let key   = ['user', 'phone', 'email', 'photo']
  
          AsyncStorage.multiRemove(key).then(async res =>{
            await AsyncStorage.setItem('user', response.data.user.name)
            await AsyncStorage.setItem('phone', response.data.user.phone)
            await AsyncStorage.setItem('email', response.data.user.email)
            await AsyncStorage.setItem('photo', response.data.user.image)
          })
        }
        else {
          that.setState({ loading: false })
          Toast.show({
            text: response.data.message,
            buttonText: 'Ok',
            duration: 8000,
            textStyle: { fontSize: 12 },
            buttonStyle: { backgroundColor: '#ad59be' },
            buttonTextStyle: { fontSize: 12 }
          })
        }
      })
      .catch(function (error) {
        that.setState({ loading: false })
        Toast.show({
          text: error.message,
          buttonText: 'Ok',
          duration: 8000,
          textStyle: { fontSize: 12 },
          buttonStyle: { backgroundColor: '#ad59be' },
          buttonTextStyle: { fontSize: 12 }
        })
      });
    }
    else{
      Toast.show({
        text: 'No puedes dejar campos vacios.',
        buttonText: 'Ok',
        duration: 3000,
        textStyle: { fontSize: 12},
        buttonStyle: { backgroundColor: '#ad59be'},
        buttonTextStyle: { fontSize: 12 }
      })
    }
  }
}
