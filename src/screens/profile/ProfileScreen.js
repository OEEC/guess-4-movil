import React, { Component } from 'react'
import {
  View,
  ImageBackground,
  Text,
  Image,
  TouchableOpacity, Slider, Dimensions, AsyncStorage } from 'react-native'
import { Container, Content, Toast, ProgressBar, Thumbnail } from 'native-base'
import { URL } from '../../config/env'
import * as Progress from 'react-native-progress'
import Header   from '../../components/HeaderComponent'
import style    from './ProfileStyleSheet'
import axios    from 'axios'
import moment   from 'moment'
import Spinner  from 'react-native-loading-spinner-overlay'

let { width } = Dimensions.get('screen')


export default class ProfileScreen extends Component {

  static navigationOptions = {
    drawerLabel: 'Ver perfil'
  }

  constructor(props) {
    super(props)
    this.state = {
      winnerGames:  0,
      progress:     0,
      loadData:     false,
      update:       true,
      photoURL:     null,
      pcode:        "",
      perscode:     "",
      username:     "",
      
    }
  }

  componentDidMount() {
    this.getUserLocal()
    this.getpersonalcode()
  }

  async getUserLocal() {
    let idUser  = await AsyncStorage.getItem('id')
    let url     = await AsyncStorage.getItem('photo')
    let user    = await AsyncStorage.getItem('user')
    let email   = await AsyncStorage.getItem('email')
    let phone   = await AsyncStorage.getItem('phone')
    const that  = this
    
    this.setState({ loadData: true })
    this.setState({ photoURL: url })
    this.setState({ user : user })
    this.setState({ email: email })
    this.setState({ phone: phone })
    // ${URL}
    axios.post(`${URL}/get-my-progress`, {
      id: idUser
    })
    .then(function(response) {
      that.setState({ winnerGames:response.data.victories })
      let progress = response.data.money/250
      if(progress < 1){
        that.setState({ progress: 1 })
      }
      else if(progress < 2){
        that.setState({ progress: .8 })
      }
      else if(progress < 3){
        that.setState({ progress: .7 })
      }
      else if(progress < 4){
        that.setState({ progress: .6 })
      }
      else if(progress < 5){
        that.setState({ progress: .5 })
      }
      else if(progress < 6){
        that.setState({ progress: .4 })
      }
      else if(progress < 7){
        that.setState({ progress: .3 })
      }
      else if(progress < 8){
        that.setState({ progress: .2 })
      }
      else if(progress < 9){
        that.setState({ progress: .1 })
      }
      else{
        that.setState({ progress: 0 })
      }
      console.log(response.data.money);
      console.log(progress);
      
    })
    .catch(function (error) {
      console.log(error)
    });
  }

  getpersonalcode = async () => {
        this.state.username   = await AsyncStorage.getItem('user')
        this.state.pcode =this.state.username + "G4"
        await AsyncStorage.setItem('pcode',  this.state.pcode);
        this.state.perscode   = await AsyncStorage.getItem('pcode')
  
        let that = this
      
        let code = { 'perscode': that.state.perscode  }
        axios({
          method:'POST',
          url: `${URL}/add-personalcode`,
          headers: {
            'Content-Type': 'application/json'
          },
          data: code
        })
        .then(async function (response) {
        
           
            console.log( that.state.pcode )
              
        })
        .catch(function (error) {
          Toast.show({
            text: error.message,
            buttonText: 'Ok',
            duration: 8000,
            textStyle: { fontSize: 12 },
            buttonStyle: { backgroundColor: '#ad59be' },
            buttonTextStyle: { fontSize: 12 }
          })
        });
     
  }


  render() {
    let { navigation } = this.props
    return (
      <Container>
        <ImageBackground source={require('../../assets/images/fondo-min.png')} style={{width: '100%', height: '100%'}}>
          <Header navigation={navigation}/>
          <Content>
            <View style={style.container}>
              <View style={style.card}>
                <Thumbnail large source={{ uri:  `${URL}/assets/images/users/${this.state.photoURL}`}} />
                <TouchableOpacity
                  style={{marginTop: '8%', padding: 6, borderRadius: 4, borderColor: 'rgb(122, 0, 129)', borderWidth: 1}}
                  onPress={ this.goEditProfile }>
                  <Text style={{color: 'rgb(122, 0, 129)', fontSize: 10}}>Editar</Text>
                </TouchableOpacity>
                <View style={{flex: 1, alignItems: 'center', marginTop: '5%'}}>
                  <Text style={style.textUsername}>{ this.state.user }</Text>
                  <Text style={style.textGray}>{ this.state.email }</Text>
                  <Text style={style.textGray}>{this.state.phone }</Text>
                </View>
              </View>
              <View style={style.cardcode}>
                <Text>¡Comparte tu codigo personal con tus amigos!</Text>
                  <View style={{flex: 1, alignItems: 'center', marginTop: '1%'}}>
                       <Text style={style.textPersonalcode}>{this.state.perscode}</Text>
                  </View>
              </View>
              <View style={style.cardProgress}>
                <View style={style.boxProgress}>
                  <Text style={style.textLeft}>$0</Text>
                  <Progress.Bar
                    progress={this.state.progress}
                    borderRadius={70}
                    width={width-130}
                    color={'rgb(122, 0, 129)'}
                    style={{backgroundColor: '#e9e7e7'}}
                    height={100} 
                    borderColor={'transparent'}>
                  </Progress.Bar>
                  <Text style={style.textRight}>$250</Text>
                </View>
                <View style={style.boxTextProgress}>
                  <Text style={style.textPrize}>Junta $250 para que se te</Text>
                  <Text style={[style.textPrize, {marginTop: -5}]}>haga el deposito</Text>
                </View>
                <View style={style.boxTextProgress}>
                  <Text style={style.textGray}>Juegos ganados</Text>
                  <Text style={style.textNumber}>{this.state.winnerGames}</Text>
                </View>
              </View>
            </View>
          </Content>
        </ImageBackground>
      </Container>
    )
  }

  goEditProfile = () => {
    let { navigate } = this.props.navigation
    navigate('EditProfile')
  }
}
