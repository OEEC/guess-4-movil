import { StyleSheet } from 'react-native'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen'

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    justifyContent: 'space-between'
  },
  card: {
    flex: 2,
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: '#FFF',
    borderRadius: 10,
    padding: 40,
    height: 300
  },
  cardcode: {
    flex: 2,
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: '#FFF',
    borderRadius: 10,
    padding: 40,
    height: 150,
    marginTop: '5%'
  },
  profilePhoto: {
    borderRadius: 100,
    width: 100,
    height: 100,
    marginBottom: '10%'
  },
  textUsername: {
    fontSize: 18,
    color: '#532b64',
    fontWeight: 'bold',
    marginTop: 5
  },
  textPersonalcode:{
    fontSize: 18,
    color: 'rgb(122, 0, 129)',
    fontWeight: 'bold',
    marginTop: 5
  },
  textGray: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#b2b2b2',
    marginTop: 5
  },
  textNumber: {
    fontWeight: 'bold',
    fontSize: 30,
    color: '#532b64'
  },
  cardProgress: {
    flex: 1,
    marginTop: '5%',
    backgroundColor: '#FFF',
    borderRadius: 4,
    padding: 20,
    height: hp('30%')
  },
  boxProgress: {
    flex: 1,
    flexDirection: 'row'
  },
  textLeft: {
    marginRight: '1%',
    fontSize: 14,
    color: '#532b64',
    fontWeight: 'bold',
    textAlignVertical: 'center'
  },
  textRight: {
    marginLeft: '1%',
    fontSize: 14,
    color: '#532b64',
    fontWeight: 'bold',
    textAlignVertical: 'center'
  },
  boxTextProgress: {
    flex: 1,
    alignItems: 'center',
    marginTop: '5%',

  },
  textPrize: {
    color: '#b2b2b2',
    fontWeight: '400'
  }
})
