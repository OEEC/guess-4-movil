import { Text, StyleSheet, View } from 'react-native'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen'

export default StyleSheet.create({
  menuIcon: {
    margin: 15,
    color: '#FAFAFA'
  },
  titleHeader: {
    margin: 15,
    color: '#FAFAFA',
    fontWeight: '900',
    fontSize: 14,
    transform: [{scaleY: 2}]
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    padding: 20
  },
  boxHistory: {
    marginBottom: hp('3%')
  },
  boxPatterns: {

  },
  textMyPattern: {
    color: '#FFF',
    fontWeight: 'bold',
    fontSize: 16
  },
  textNoWin: {
    color: '#000',
    fontWeight: 'bold',
    fontSize: 16
  },
  card: {
    flex: 1,
    marginTop: 10,
    backgroundColor: '#FFF',
    borderRadius: 10,
    padding: 10,
    flexDirection: 'row'
  },
  pattern: {
    flex: 1,
    flexDirection: 'row'
  },
  textDate: {
    color: '#b2b2b2',
    fontWeight: '700',
    fontSize: 10
  },
  textWon: {
    color: '#532b64',
    fontWeight: 'bold',
    fontSize: 12
  },
  textLost: {
    color: '#b2b2b2',
    fontWeight: 'bold',
    fontSize: 10
  },
  imagePattern: {width: wp('20%'), height: hp('10%'), resizeMode: 'contain'},
  spinnerTextStyle: {
    color: '#FFF'
  },
})
