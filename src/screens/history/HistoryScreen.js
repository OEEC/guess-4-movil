import React, { Component } from 'react'
import { URL }              from '../../config/env'
import { View, Image, Text, ImageBackground, TouchableOpacity, FlatList, ScrollView, AsyncStorage } from 'react-native'
import { Container, Content, Toast} from 'native-base'
import HeaderComponent from '../../components/HeaderComponent'
import style    from './HistoryStyleSheet'
import axios    from 'axios'
import moment   from 'moment'
import Spinner  from 'react-native-loading-spinner-overlay'


export default class HistoryScreen extends Component {

  static navigationOptions = {
    drawerLabel: 'Historial'
  }

  constructor(props) {
    super(props)

    this.state = {
      data:     [],
      winners:  [],
      all:      [],
      winner:   [],
      id:       '14',
      loading:  true
    }
  }

  componentDidMount() {
    this.history()
  }

  history = async () => {
    let { navigate }  = this.props.navigation;
    const that        = this
    const historyPush = []
    const allPush     = []
    
    axios.post(`${URL}/my-history`, {
      id: await AsyncStorage.getItem('id')
    })
    .then(async function (response) {
      moment.locale('es')
      that.setState({loading:false})

      response.data.history.map(function(history){
        const date  = moment(history.date).format('MMMM Do YYYY')
        const h     = JSON.parse(history.code)

        this.hTemp = { 'id':history.id, 'date':date, 'c1':h[0].image, 'c2':h[1].image, 'c3':h[2].image, 'c4':h[3].image }
        historyPush.push(this.hTemp)
      })

      response.data.all.map(function(history){
        const date  = moment(history.date).format('MMMM Do YYYY')
        const h     = JSON.parse(history.code)

        this.hTemp = { 'id':history.id, 'date':date, 'c1':h[0].src, 'c2':h[1].src, 'c3':h[2].src, 'c4':h[3].src }
        allPush.push(this.hTemp)
      })
      
      that.setState({ all: allPush })
      that.setState({ winner: historyPush })
    })
    .catch(function (error) {
      that.setState({loading:false})
      console.log(error)
    });
  }

  render() {
    let { navigation } = this.props;
    let that = this
    return (
        <Container>
          <ImageBackground source={require('../../assets/images/fondo-min.png')} style={{width: '100%', height: '100%'}}>
            <Spinner
              visible={this.state.loading}
              textContent={'Espera!!!...'}
              textStyle={style.spinnerTextStyle}
            />
            <HeaderComponent navigation={navigation}/>
            <Content>
              <View style={style.container}>
                <View style={style.boxHistory}>
                  <Text style={style.textMyPattern}>Partidas Ganadas</Text>
                  { this.state.winner == [] ? 
                    <View style={style.card}>
                      <Text style={style.textNoWin}>No tienes partidas ganadas</Text>
                    </View> :
                    <FlatList
                      data={this.state.winner}
                      renderItem={({item}) =>
                        <View style={style.card}>
                          <View>
                            <Text style={style.textDate}>{item.date}</Text>
                          </View>
                          <ScrollView style={style.pattern} horizontal={true} showsHorizontalScrollIndicator={true}>
                            <Image key={item.c1} source={ {uri:`${URL}/assets/images/brands/${item.c1}` }} style={style.imagePattern}></Image>
                            <Image key={item.c2} source={ {uri:`${URL}/assets/images/brands/${item.c2}` }} style={style.imagePattern}></Image>
                            <Image key={item.c3} source={ {uri:`${URL}/assets/images/brands/${item.c3}` }} style={style.imagePattern}></Image>
                            <Image key={item.c4} source={ {uri:`${URL}/assets/images/brands/${item.c4}` }} style={style.imagePattern}></Image>
                          </ScrollView>
                        </View>
                      }
                      keyExtractor={(value, index) => index.toString()}
                    /> 
                  }
                  
                </View>
                <View style={style.boxHistory}>
                  <Text style={style.textMyPattern}>Otras Partidas</Text>
                  { this.state.winner == [] ? 
                    <View style={style.card}>
                      <Text style={style.textNoWin}>No tienes un historial</Text>
                    </View> :
                    <FlatList
                      data={this.state.all}
                      renderItem={
                        ({item}) =>
                          <View style={style.card}>
                            <View>
                              <Text style={style.textDate}>{item.date}</Text>
                            </View>
                            <ScrollView style={style.pattern} horizontal={true} showsHorizontalScrollIndicator={true}>
                              <Image key={item.c1} source={ {uri:`${URL}/assets/images/brands/${item.c1}` }} style={style.imagePattern}></Image>
                              <Image key={item.c2} source={ {uri:`${URL}/assets/images/brands/${item.c2}` }} style={style.imagePattern}></Image>
                              <Image key={item.c3} source={ {uri:`${URL}/assets/images/brands/${item.c3}` }} style={style.imagePattern}></Image>
                              <Image key={item.c4} source={ {uri:`${URL}/assets/images/brands/${item.c4}` }} style={style.imagePattern}></Image>
                            </ScrollView>
                          </View>
                      }
                      keyExtractor={(value, index) => index.toString()}
                    />
                  }
                </View>
              </View>
            </Content>
          </ImageBackground>
        </Container>
    )
  }
}
