import { StyleSheet } from 'react-native'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen'

export default StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    flexDirection: 'column'
  },
  viewContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  boxContainerLogo: {
    flex: 4,
    flexDirection: 'row',
    alignItems: 'center'
  },
  logo: {
    width: wp('80%'),
    height: hp('20%')
  },
  boxContainerText: {
    flex: 1,
    marginTop: -20
  },
  boxContainerTextTime: {
    flex: 1,
    marginTop: -30,
    marginEnd: 20
  },
  boxContainerButton: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginTop:20
  },
  text: {
    fontSize: 20,
    color: '#FAFAFA',
    textAlign: 'center'
  },
  button: {
    backgroundColor: '#a903bd',
    borderRadius: 40
  },
  textButton: {
    marginStart: 85,
    marginEnd: 85,
    margin: 12,
    fontSize: 14,
    color: '#ffffff',
    fontWeight: 'bold',
  }
})
