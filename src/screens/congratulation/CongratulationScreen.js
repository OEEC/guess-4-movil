import React, { Component } from 'react';
import { View, TouchableOpacity, Image, ImageBackground } from 'react-native';
import { Button, Text, Toast } from 'native-base';
import Style from './CongratulationStyleSheet';
import ConfettiCannon from 'react-native-confetti-cannon'
import moment from 'moment'
import axios  from 'react-native-axios'
import {URL}  from '../../config/env'

export default class StartScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      time: '',
      actual: ''
    }
  }

  componentWillMount(){
    this.getCode()
  }

  getCode() {
    const that = this
    
    axios.get(`${URL}/actual-code`)
    .then(function (response) {
      let code = response.data

      code.forEach(time => {
        const day = moment(time.date)
        const now = moment()

        day.locale('es')
        now.locale('es')

        const nowDate = now.format('HH:mm a')
        const date    = day.format('HH:mm a')
        
        that.setState({ actual: nowDate})
        that.setState({ time:date })
      });
      
    })
    .catch(function (error) {
      console.log(error);
      
      Toast.show({
        text: 'Lo sentimos, el código esta en conflictos',
        buttonText: 'Ok',
        duration: 3000,
        textStyle: { fontSize: 12},
        buttonStyle: { backgroundColor: '#ad59be'},
        buttonTextStyle: { fontSize: 12 }
      })
    });
  }  

  render() {
    return (
      <ImageBackground source={require('../../assets/images/fondo-min.png')} style={Style.container}>
        <ConfettiCannon count={500} origin={{x: -20, y: 10}} fallSpeed={3000} />
        <View style={Style.viewContainer}>
          <View style={Style.boxContainerLogo}>
            <Image source={require('../../assets/images/guess4-logo-1024-min.png')} style={Style.logo} ></Image>
          </View>
          <View style={Style.boxContainerText}>
            <Text style={Style.text}>¡Perfecto!</Text>
            <Text style={[Style.text, {marginTop: -1}]}>Ahora solo queda esperar al ganador</Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <View style={Style.boxContainerTextTime}>
              <Text style={Style.text}>Tiempo de aviso</Text>
              <Text style={[Style.text, {marginTop: -1}]}>{this.state.time}</Text>
            </View>
            <View style={Style.boxContainerTextTime}>
              <Text style={Style.text}>Tiempo actual</Text>
              <Text style={[Style.text, {marginTop: -1}]}>{this.state.actual}</Text>
            </View>
          </View>
          <View style={Style.boxContainerButton}>
            <TouchableOpacity onPress={this.congratulation.bind(this)} style={Style.button}>
              <Text style={Style.textButton}>Volver</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    )
  }

  congratulation = () => {
    this.props.navigation.navigate('Home')
  }
}