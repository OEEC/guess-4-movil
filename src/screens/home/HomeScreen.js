import React, { Component } from 'react'
import { TouchableOpacity } from 'react-native'
import { View, Text, ImageBackground, Image, FlatList, Animated, Easing, AsyncStorage, Modal, Button } from 'react-native'
import { Container, Content, Icon, Toast } from 'native-base'
import HeaderComponent from '../../components/HeaderComponent'
import style    from './HomeStyleSheet'
import axios    from 'react-native-axios'
import moment   from 'moment/min/moment-with-locales'
import {URL, URLalter}    from '../../config/env'
import Spinner  from 'react-native-loading-spinner-overlay'


export default class HomeScreen extends Component {

  constructor(props) {
    super(props)
    this.state = {
      pattern:  this.props.navigation.getParam('selectedBrands'),
      code:     [],
      myCode:   [],
      statusCode: [],
      loading:  true,
      ctext: "",
      mtext: "",
      m2text: "",
      modalVisible: false,
      message: ""
    }
    this.animatedValue = new Animated.Value(0)
  }

  componentDidMount(){
    this.getCode()
    this.animate()
    this.eventCode()
    this.getCodeWinner()
    this.getvalidate()
  }

  animate() {
    this.animatedValue.setValue(0)
    Animated.timing(
      this.animatedValue, 
      {
        toValue: 1,
        duration: 900,
        easing: Easing.linear
      }
    ).start()
  }

  getCode() {
    const that = this

    axios.get(`${URL}/actual-code`)
    .then(async function (response) {
      that.setState({loading:false})
      const data = response.data
      that.setState({ code: data })
    })
    .catch(function (error) {
      that.setState({loading:false})
      Toast.show({
        text: error.message,
        buttonText: 'Ok',
        duration: 3000,
        textStyle: { fontSize: 12},
        buttonStyle: { backgroundColor: '#ad59be'},
        buttonTextStyle: { fontSize: 12 }
      })
    });
  }

  getCodeWinner = async() => {
    const that = this
    
    AsyncStorage.getItem('id').then(id => {
      axios({
        method:'POST',
        url: `${URL}/get-my-code`,
        headers: {
          'Content-Type': 'application/json'
        },
        data: { id_user: id }
      })
      .then(function (response) {
        if (response.data.success) {
          that.setState({ myCode:JSON.parse(response.data.combination.combination)})
        }8
      })
      .catch(function (error) {
        
      });
    })
  }

  getvalidate = async () => {
    let that = this
    
    const user_id   = await AsyncStorage.getItem('id')
    const code_id   = await AsyncStorage.getItem('idprom')
    let code = {'idu': user_id, 'idc': code_id}
   	axios({
      method:'POST',
      url: `${URL}/get-usercode`,
      headers: {
        'Content-Type': 'application/json'
      },
      data: code
    })
    .then(async function (response) {
      console.log(response.data)
      that.setState({statusCode: response.data.validate})
      // that.setState({message: response.data.message})
      console.log(that.state.message)
      console.log(that.state.statusCode)
      console.log(that.state.code)

      if(response.data.message=='Codigo Encontrado'){
         console.log('con codigos')
         that.state.ctext="¡Genial!"
         that.state.mtext=response.data.message
         that.state.m2text=''
         that.setModalVisible(true);
      
      }else 
      if(response.data.message=='No hay codigos de promocion introducidos'){
        console.log('sin codigo')
        that.state.ctext="¡UPS!"
        that.state.mtext='No hay codigos de promocion'
        that.state.m2text='introducidos'
        that.setModalVisible(true);
        
        }
    //   Toast.show({
    //     text: response.data.message,
    //     buttonText: 'Ok',
    //     duration: 8000,
    //     textStyle: { fontSize: 12 },
    //     buttonStyle: { backgroundColor: '#ad59be' },
    //     buttonTextStyle: { fontSize: 12 }
    //   })
     })
    .catch(function (error) {
      // Toast.show({
      //   text: error.message,
      //   buttonText: 'Ok',
      //   duration: 8000,
      //   textStyle: { fontSize: 12 },
      //   buttonStyle: { backgroundColor: '#ad59be' },
      //   buttonTextStyle: { fontSize: 12 }
      // })
        that.state.ctext="¡UPS!"
        that.state.mtext=error.message
        that.state.m2text=''
        that.setModalVisible(true);
    });
  
  }


  eventCode() {
    const { code }  = this.state

    if(code != []){
      return code.map(function(element){
        const m     = moment(element.date)

        m.locale('es')

        const date  = m.format('D MMMM HH:mm a')

        return(
          <View style={{ flex: 1, flexDirection: 'column' }}>
            <Text key={element.id} style={style.textMessageBold}>{date}</Text>
            <Text key={element.money} style={style.textMessageBold}>Participas por: ${element.money}</Text>
          </View>
        )
      })
    }
    else{
      return <Text key={'10929192'} style={style.textMessageBold}>Aun no se ha generado un patrón ganador</Text>
    }
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    let opacity = this.animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 1, 1]
    })
    let marginTop = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [-40, 0]
    })
    let scale = this.animatedValue.interpolate({
      inputRange: [0, 0.3],
      outputRange: [0.4, 0.58]
    })
    let {navigation} = this.props;
    return (
        <Container>
          <ImageBackground source={require('../../assets/images/fondo-min.png')} 
          style={{width: '100%', height: '100%'}}>
            <Spinner
              visible={this.state.loading}
              textContent={'Cargando juego...'}
              textStyle={style.spinnerTextStyle}
            />
            <HeaderComponent navigation={navigation}/>
            <Content>
              <View style={style.container}>
                <Animated.View style={[style.boxContainerLogo, { opacity }]}>
                  <Image source={require('../../assets/images/guess4-logo-1024-min.png')} style={style.logo}/>
                </Animated.View>

                <View style={style.cardMessage} >
                    <Text style={[style.textMessage, {fontFamily: 'Roboto'}]}>¡Estas participando!</Text>
                    <Text style={[{marginBottom: '8%'}, style.textMessage]}>Los resultados se publicarán el día</Text>

                    {this.eventCode()}
                </View>
                <View style={style.boxPattern}>
                  <Text style={style.textPattern}>Patrón seleccionado</Text>
                </View>
                <Animated.View style={[style.cardPattern]}>
                  { this.state.code != [] ? <FlatList 
                    data={this.state.myCode}
                    renderItem={({item}) => {
                        if(item.state){
                          return(
                            <Image key={item.src} resizeMode="contain" source={{ uri: `${URL}/assets/images/brands/${item.src}` }} style={style.brand}/>
                          )
                        }
                      }
                    }
                    keyExtractor={(value, index) => index.toString()}
                    numColumns={4}
                  /> : 
                  <Text key={'12388321'} style={[style.textMessage, {fontFamily: 'Roboto'}]}>¡Aun no has seleccionado un patrón!</Text>}
                </Animated.View>
                { this.state.code ? <TouchableOpacity style={{justifsyContent: 'center', flex: 1, alignItems: 'center'}} onPress={this.showDialog}>
                  <Icon name="gamepad" type="FontAwesome" style={style.iconShare}/>
                  <Text style={{color: '#FFF', fontWeight: 'bold', fontStyle: 'italic' }}>Jugar Ahora</Text>
                </TouchableOpacity> : <View/> }
              </View>
              {/* Modal */}
              <View style={style.containerModal}>
                <Modal
                  animationType="slide"
                  transparent={true}
                  visible={this.state.modalVisible}>
                  <View style={style.modalContainer}>
                    <View style={style.innerContainer}>
                    <View>
                    <Text style={[style.textTile, {fontFamily: 'Roboto'}]}>{this.state.ctext}</Text>
                    </View>
                      <Text style={[style.textMessageModal, {fontFamily: 'Roboto'}]}>{this.state.mtext}</Text>
                      <Text style={[style.textMessage2, {fontFamily: 'Roboto'}]}>{this.state.m2text}</Text>

                      <TouchableOpacity
                          onPress={ () =>{ this.setModalVisible(!this.state.modalVisible)}  }
                          style={style.button}> 
                          <Text style={style.textButton}>Aceptar</Text>
                        </TouchableOpacity> 
                    
                      {/* <Button style={ style.button} onPress={() => {
                          this.setModalVisible(!this.state.modalVisible);}} title="Aceptar">
								    	
								    	</Button> */}
                  
                    </View>
                  </View>
                </Modal>
                </View>
            </Content>
          </ImageBackground>
        </Container>
    )
  }

  showDialog = async () => {
    let {navigate}  = this.props.navigation
    let codes=JSON.parse(this.state.code[0].code)
    console.log('showdialog')
    console.log(codes)
    if(this.state.statusCode.length>0){
      console.log('if statuscode')
      console.log(this.state.statusCode)
        this.state.statusCode.map(status => {
  
          if(status.personal_code==0){
            navigate('Game', {selectedBrands: [
              { id: codes[0].id, src: codes[0].image, video: null, state: true, add: false},
              { id: null, src: null, video: null, state: false, add: false },
              { id: null, src: null, video: null, state: false, add: false},
              { id: null, src: null, video: null, state: false, add: false}
            ], id: this.state.code[0].id})

          }else if(status.personal_code==1){
            navigate('Game', {selectedBrands: [
              { id: code[0], src: codes[0], video: null, state: false, add: false},
              { id: code[1], src: codes[1], video: null, state: false, add: false},
              { id: null, src: null, video: true, state: false, add: false},
              { id: null, src: null, video: true, state: false, add: false}
            ], id: this.state.code[0].id})
          }
        
        })
      }else{
        navigate('Game', {selectedBrands: [
        { id: null, src: null, video: null, state: false, add: false, code: true },
        { id: null, src: null, video: null, state: false, add: false, code: false },
        { id: null, src: null, video: null, state: false, add: false, code: false },
        { id: null, src: null, video: null, state: false, add: false, code: false}
      ], id: this.state.code[0].id})
    }
     }
}
