import { StyleSheet } from 'react-native'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol
} from 'react-native-responsive-screen'

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    alignContent: 'center'
  },
  boxContainerLogo: {
    flex: 1,
    alignItems: 'center',
    marginTop: wp('-12%')
  },
  cardMessage: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#FFF',
    borderRadius: 10,
    padding: 30,
    marginBottom: wp('5%')
  },
  cardPattern: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#FFF',
    borderRadius: 10,
    paddingVertical: 12,
    paddingHorizontal: 25,
  },
  textPattern: {
    color: '#FFF',
    fontWeight: 'bold',
    fontSize: 14
  },
  logo: {
    width: wp('70%'),
    height: hp('30%')
  },
  brand: {
    width: wp('15%'),
    height: wp('15%'),
    padding: -50
  },
  textMessage: {
    color: '#b2b2b2'
  },
  textMessageBold: {
    fontWeight: 'bold',
    color: '#b2b2b2',
    fontSize: wp('5%')
  }, 
  textPattern: {
    color: '#FFF', 
    fontSize: wp('4.5%'),
    fontWeight: 'bold', 
    marginTop: wp('4%'),
    marginBottom: wp('2%')
  },
  boxPattern: {
    flexDirection: 'row', 
    justifyContent: 'space-between'
  },
  iconShare: {
    color: '#FFF',
    fontSize: wp('8%'),
    marginTop: hp('2%')
  },
  imageBrand: {
    width: 65,
    height: 65,
    borderRadius: 100,
    overflow: 'hidden',
    backgroundColor: '#ffffff',
    padding: 100
  },
  spinnerTextStyle: {
    color: '#FFF'
  },
  containerModal: {
    flex: 1,
    textAlign: 'center',
    justifyContent: 'center',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  innerContainer: {
    backgroundColor: '#FFF',
    borderRadius: 10,
    borderColor: '#6f42c1',
    borderStyle: 'solid',
    borderWidth: 2,
    paddingVertical: 12,
    paddingHorizontal: 25,
    alignItems: 'center',
  
  },
  textTile:{
    fontSize: 18,
    color: 'rgb(122, 0, 129)',
    fontWeight: 'bold',
    marginTop: 1,
    paddingTop: 2,
    paddingBottom: 3
  },
  textMessageModal:{
    fontSize: 20,
    color: 'black',
    fontWeight: 'bold',
    paddingTop: 5,
 },
 button: {
  borderRadius: 40,
  backgroundColor: '#a903bd',
  marginTop: 25

},
textButton: {
  marginStart: 85,
  marginEnd: 85,
  margin: 12,
  fontSize: 11,
  color: '#ffffff',
  fontWeight: 'bold',
  textAlign: 'center'
},

textMessage2:{
  fontSize: 20,
  color: 'black',
  fontWeight: 'bold',
  paddingBottom: 5
},
})
