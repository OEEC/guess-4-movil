import React, { Component } from 'react'
import { View, Image, ImageBackground, StyleSheet, Dimensions, AsyncStorage } from 'react-native'
import { Toast } from 'native-base'
import background from '../assets/images/guess4-logo-1024-min.png'
import * as Progress from 'react-native-progress'

let { width } = Dimensions.get('window')

export default class LoadingComponent extends Component {
  
  constructor(props) {
    super(props)
  }

  async componentDidMount() {
    let {navigate} = this.props.navigation
    let user = await AsyncStorage.getItem('token')
    
    if(user != null){
      navigate('Home')
    }
    else{
      navigate('Login')
    }
  }

  render() {
    return(
      <ImageBackground source={require('../assets/images/fondo-min.png')} style={[{width: '100%', height: '100%'}, style.container]}>
        <Image source={background} style={{width: '50%', height: '15%'}}/>
        <Progress.Bar
          progress={0.5}
          indeterminate={true}
          height={20}
          width={200}
          borderRadius={50}
          color={'rgb(122, 0, 129)'}
          style={{backgroundColor: '#C7C5C5'}}  borderColor={'transparent'}/>
      </ImageBackground>
    )
  }
}

let style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20
  }
})
