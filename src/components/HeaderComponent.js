import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { Header, Left, Icon, Right, Body} from 'native-base'
import style from '../screens/history/HistoryStyleSheet'

export default class HeaderComponent extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    let {navigation} = this.props;
    return (
      <Header transparent androidStatusBarColor="#532b64" iosBarStyle="light-content">
        <Left>
          <TouchableOpacity onPress={() => {
            navigation.toggleDrawer()
          }}>
            <Icon name="menu" style={style.menuIcon}></Icon>
          </TouchableOpacity>
        </Left>
        <Body />
      </Header>
    )
  }
}
