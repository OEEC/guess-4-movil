/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, AppRegistry, ImageBackground} from 'react-native';
import { Root } from 'native-base'
import Navigator from './src/config/routes';
import OneSignal from 'react-native-onesignal';

type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props)
    console.disableYellowBox = true
    OneSignal.init('99ff90d0-35ff-45d2-b5e2-ef7d3ab6e667')
  }
  render() {
    return (
      <Root>
        <Navigator />
      </Root>
    );
  }
}
